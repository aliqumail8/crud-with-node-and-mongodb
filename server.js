const express = require("express");
const mongoose = require("mongoose");
const Country = require("./models/country.model");
const bodyParser = require("body-parser");
const ejs = require("ejs");

const app = express();

app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("public"));

mongoose.connect('mongodb://127.0.0.1:27017/countryDB');

app.get("/", async (req, res) => {
  let countries = await Country.find();
  // res.render("main", { countries: countries });
  console.log(countries);
  res.send("it's working");
});

app.post("/post", (req, res) => {
  console.log(req.body);
  Country.create(
    {
      id: req.body.id,
      name: req.body.name,
      capital: req.body.capital,
    },
    function (err) {
      if (err) return handleError(err);
    }
  );
  res.redirect("/");
});

app.put("/put/:id", (req, res) => {
  const { id } = req.params;
  Country.updateOne(
    { id: id },
    { name: req.body.name, capital: req.body.capital },
    function (err, docs) {
      if (err) {
        console.log(err);
      } else {
        console.log("Updated", docs);
      }
    }
  );
  res.redirect("/");
});

app.delete("/delete/:id", (req, res) => {
  const { id } = req.params;
  Country.deleteOne({ id: id }, function (err) {
    if (err) return handleError(err);
  });
  res.redirect("/");
});

app.patch("/patch/:id", (req, res) => {
  const { id } = req.params;
  let q1 = {}
  Country.updateOne(
    { id: id },
    { capital: req.body.capital },
    function (err, docs) {
      if (err) {
        console.log(err);
      } else {
        console.log("Updated", docs);
      }
    }
  );
  res.redirect("/");
});

app.post("/findbyid",(req,res)=>{
    let _id = req.body._id;
    Country.findById(_id, function (err, docs) {
      if (err){
          console.log(err);
      }
      else{
          console.log("Result : ", docs);
      }
  });
  res.redirect("/");
});

app.listen(3000, () => {
  console.log("Running on port 3000");
});
