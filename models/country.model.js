const mongoose = require("mongoose");

const Country = new mongoose.Schema({
  id: {type: Number, required:true, unique: true},
  name: { type: String, required: true },
  capital: { type: String, required: true },
});

const model = mongoose.model("CountryData",  Country);

module.exports = model;